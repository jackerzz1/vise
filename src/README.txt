# VGG Image Search Engine (VISE)
VGG Image Search Engine (VISE) is a standalone application for 
visual search of images. VISE has been developed by the 
Visual Geometry Group (VGG) and released under the BSD license 
which allows it to be useful for both academic projects and 
commercial applications.

VISE builds on the C++ codebase developed by Relja Arandjelovic 
during his DPhil / Postdoc at the Visual Geometry Group, 
Department of Engineering Science, University of Oxford. 
VISE is developed and maintained by Abhishek Dutta.

For more details, visit https://www.robots.ox.ac.uk/~vgg/software/vise/